const express = require("express");
const Service = require("../../models/service");
const { check, validationResult } = require("express-validator");
const router = express.Router();
const auth = require("../../middleaware/auth");
const MSGS = require("../../messages");
const file = require("../../middleaware/file");
const config = require("config");
const complete_link = require("../../service/complete_link");
const get_max_order = require("../../service/get_max_order");

// @route    POST /services/:serviceId
// @desc     CREATE services
// @access   Private
router.post("/", auth, file, async (req, res, next) => {
  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    } else {
      req.body.photo = `services/${req.body.photo_name}`;
      console.log(req.body);
      let service = new Service(req.body);
      service.last_modified_by = req.user.id;
      await service.save();
      if (service.id) {
        const BUCKET_PUBLIC_PATH =
          process.env.BUCKET_PUBLIC_PATH || config.get("BUCKET_PUBLIC_PATH");
        service.photo = `${BUCKET_PUBLIC_PATH}${service.photo}`;
        res.json(service);
      }
    }
  } catch (err) {
    console.error(err.message);
    res.status(500).send({ error: MSGS.GENERIC_ERROR });
  }
});

// @route    PATCH /services/:serviceId
// @desc     UPDATE services
// @access   Private

router.patch("/:serviceId", auth, file, async (req, res, next) => {
  try {
    // const serviceId = req.params.serviceId
    const serviceId = req.params.serviceId;
    let query = { "services.service._id": serviceId };
    update = {};
    if (req.body.photo_name) {
      req.body.photo = `services/${req.body.photo_name}`;
    }
    for (const [key, value] of Object.entries(req.body)) {
      update[`services.service.$.${key}`] = value;
    }
    await Service.updateOne(query, { $set: update }, { new: true });
    let service = await Service.findOne(query);
    if (service.id) {
      service = complete_link(service);
      res.json(service);
    } else {
      res.status(404).send({ error: MSGS.CONTENT404 });
    }
  } catch (err) {
    console.error(err.message);
    res.status(500).send({ error: MSGS.GENERIC_ERROR });
  }
});

// @route    DELETE /services/:serviceId
// @desc     DELETE services
// @access   Private
router.delete("/:serviceId", auth, async (req, res, next) => {
  try {
    const id = req.params.serviceId;
    let query = { ["services.service"]: req.body };
    const service = await Service.findOneAndUpdate(
      { _id: id },
      { $pull: query },
      { new: true }
    );
    if (service) {
      res.json(service);
    } else {
      res.status(404).send({ error: MSGS.CONTENT404 });
    }
  } catch (err) {
    console.error(err.message);
    res.status(500).send({ error: MSGS.GENERIC_ERROR });
  }
});

module.exports = router;
