const mongoose = require("mongoose");

const ServiceSchema = new mongoose.Schema(
  {
    photo: {
      type: String,
      required: true,
    },
    title: {
      type: String,
      required: true,
      unique: true,
    },
    description: {
      type: String,
      required: true,
    },
    last_modified_by: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "user",
    },
    last_modification_date: {
      type: Date,
      default: Date.now,
    },
  },
  { autoCreate: true }
);

module.exports = mongoose.model("service", ServiceSchema);
