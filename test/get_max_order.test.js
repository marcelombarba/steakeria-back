const get_max_order = require ('../service/get_max_order')
const Content = require ('../models/content')
const mongoose = require('mongoose')
const connectDB= require('../config/db')


beforeAll(async () =>{
    connectDB()
})

afterAll(async () =>{
    mongoose.connection.close()
})


test('empty content in a specific,', async () => {

    let content = await Content.findOne({_id:'5fa83b671ccd1a2e3c7ce808'})
    type= 'infos'
    expect(get_max_order(content, type)).toBe(1)
})