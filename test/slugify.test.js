const slugify = require ('../service/slugify')

test('tranform string new york strip.jpg', () => {
    expect(slugify('new york strip.jpg')).toBe('new-york-strip.jpeg')
})